<?php

class Pos_Date extends DateTime
{
    protected $_year;
    protected $_month;
    protected $_day;

    public function __construct($timezone = null)
    {
        if ($timezone) {
            parent::__construct('now', $timezone);
        } else {
            parent::__construct('now');
        }

        $this->_year = (int)$this->format('Y');
        $this->month = (int)$this->format('n');
        $this->_day = (int)$this->format('j');
    }

    public function setTime($hours, $minutes, $seconds = 0)
    {
        if (!is_numeric($hours) || !is_numeric($minutes) || !is_numeric($seconds)) {
            throw new Exception('setTime() expects two or three numbers separated by commas in the order: hours, minutes, seconds.');
        }
        $outOfRange = false;
        if ($hours < 0 || $hours > 23) {
            $outOfRange = true;
        }
        if ($minutes < 0 || $minutes > 59) {
            $outOfRange = true;
        }
        if ($seconds < 0 || $seconds > 59) {
            $outOfRange = true;
        }
        if ($outOfRange) {
            throw new Exception('Invalid time.');
        }
        parent::setTime($hours, $minutes, $seconds);
    }

    public function setDate($year, $month, $day)
    {
        if (!is_numeric($year) || !is_numeric($month) || !is_numeric($day)) {
            throw new Exception('setDate expects three numbers separated by commas in the order: year, month, day.');
        }
        if (!checkdate($month, $day, $year)) {
            throw new Exception('Non-existent date.');
        }
        parent::setDate($year, $month, $day);
        $this->_year = (int) $year;
        $this->_month = (int) $month;
        $this->_day = (int) $day;
    }

    public function modify()
    {
        throw new Exception('modify() has been disabled');
    }
}
